<?php

return [
	'models' => [
		'product' => \App\Models\Product::class,
		'prospect' => \App\Models\Prospect::class,
		'customer' => \App\Models\Customer::class,
	]
];